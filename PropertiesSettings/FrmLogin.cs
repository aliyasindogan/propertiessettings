﻿using PropertiesSettings.Properties;
using System;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace PropertiesSettings
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (checkBoxRememberMe.Checked)
            {
                Settings.Default["UserName"] = txtUserName.Text;
                Settings.Default["Password"] = txtPassword.Text;
                Settings.Default.Save();
                if (!String.IsNullOrEmpty(txtApiIpAddress.Text))
                {
                    Settings.Default["SelectedApiIpAddress"] = txtApiIpAddress.Text;

                    StringCollection apiIpAddressList = new StringCollection();
                    apiIpAddressList = Settings.Default.ApiIpAddress;
                    if (!Settings.Default.ApiIpAddress.Contains(txtApiIpAddress.Text))
                        apiIpAddressList.Add(txtApiIpAddress.Text);
                    Settings.Default.ApiIpAddress = apiIpAddressList;
                    Settings.Default.Save();
                }
                else
                {
                    Settings.Default["SelectedApiIpAddress"] = comboBoxApiIpAddress.SelectedItem.ToString();
                    Settings.Default.Save();
                }
            }
            this.Hide();
            FrmMain frmMain = new FrmMain();
            frmMain.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtApiIpAddress.Visible = false;
            lblNewApiIpAddress.Visible = false;
            if (Settings.Default.ApiIpAddress != null)
            {
                foreach (var item in Settings.Default.ApiIpAddress)
                {
                    comboBoxApiIpAddress.Items.Add(item);
                }
            }

            txtPassword.Text = Settings.Default["Password"].ToString(); //bilgileri
            txtUserName.Text = Settings.Default["UserName"].ToString(); //bilgileri
        }

        private void checkBoxNewApiIpAddress_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxNewApiIpAddress.Checked)
            {
                txtApiIpAddress.Visible = true;
                lblNewApiIpAddress.Visible = true;
                comboBoxApiIpAddress.Enabled = false;
            }
            else
            {
                txtApiIpAddress.Visible = false;
                lblNewApiIpAddress.Visible = false;
                comboBoxApiIpAddress.Enabled = true;
            }
        }
    }
}