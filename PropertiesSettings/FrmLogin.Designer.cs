﻿
namespace PropertiesSettings
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtApiIpAddress = new System.Windows.Forms.TextBox();
            this.lblNewApiIpAddress = new System.Windows.Forms.Label();
            this.checkBoxNewApiIpAddress = new System.Windows.Forms.CheckBox();
            this.checkBoxRememberMe = new System.Windows.Forms.CheckBox();
            this.comboBoxApiIpAddress = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kullanıcı Adı";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Şifre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "API IP Adresi";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(35, 143);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(368, 37);
            this.btnLogin.TabIndex = 1;
            this.btnLogin.Text = "Giriş";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(35, 30);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(181, 20);
            this.txtUserName.TabIndex = 2;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(222, 30);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(181, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // txtApiIpAddress
            // 
            this.txtApiIpAddress.Location = new System.Drawing.Point(166, 213);
            this.txtApiIpAddress.Name = "txtApiIpAddress";
            this.txtApiIpAddress.Size = new System.Drawing.Size(224, 20);
            this.txtApiIpAddress.TabIndex = 2;
            // 
            // lblNewApiIpAddress
            // 
            this.lblNewApiIpAddress.AutoSize = true;
            this.lblNewApiIpAddress.Location = new System.Drawing.Point(163, 196);
            this.lblNewApiIpAddress.Name = "lblNewApiIpAddress";
            this.lblNewApiIpAddress.Size = new System.Drawing.Size(227, 13);
            this.lblNewApiIpAddress.TabIndex = 0;
            this.lblNewApiIpAddress.Text = "Yeni API IP Adresi (http://localhost:44328/api)";
            // 
            // checkBoxNewApiIpAddress
            // 
            this.checkBoxNewApiIpAddress.AutoSize = true;
            this.checkBoxNewApiIpAddress.Location = new System.Drawing.Point(35, 215);
            this.checkBoxNewApiIpAddress.Name = "checkBoxNewApiIpAddress";
            this.checkBoxNewApiIpAddress.Size = new System.Drawing.Size(110, 17);
            this.checkBoxNewApiIpAddress.TabIndex = 3;
            this.checkBoxNewApiIpAddress.Text = "Yeni API IP Adres";
            this.checkBoxNewApiIpAddress.UseVisualStyleBackColor = true;
            this.checkBoxNewApiIpAddress.CheckedChanged += new System.EventHandler(this.checkBoxNewApiIpAddress_CheckedChanged);
            // 
            // checkBoxRememberMe
            // 
            this.checkBoxRememberMe.AutoSize = true;
            this.checkBoxRememberMe.Location = new System.Drawing.Point(35, 120);
            this.checkBoxRememberMe.Name = "checkBoxRememberMe";
            this.checkBoxRememberMe.Size = new System.Drawing.Size(80, 17);
            this.checkBoxRememberMe.TabIndex = 3;
            this.checkBoxRememberMe.Text = "Beni Hatırla";
            this.checkBoxRememberMe.UseVisualStyleBackColor = true;
            // 
            // comboBoxApiIpAddress
            // 
            this.comboBoxApiIpAddress.FormattingEnabled = true;
            this.comboBoxApiIpAddress.Location = new System.Drawing.Point(35, 81);
            this.comboBoxApiIpAddress.Name = "comboBoxApiIpAddress";
            this.comboBoxApiIpAddress.Size = new System.Drawing.Size(368, 21);
            this.comboBoxApiIpAddress.TabIndex = 4;
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 248);
            this.Controls.Add(this.comboBoxApiIpAddress);
            this.Controls.Add(this.checkBoxNewApiIpAddress);
            this.Controls.Add(this.checkBoxRememberMe);
            this.Controls.Add(this.txtApiIpAddress);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblNewApiIpAddress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmLogin";
            this.ShowIcon = false;
            this.Text = "Properties Settings ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtApiIpAddress;
        private System.Windows.Forms.Label lblNewApiIpAddress;
        private System.Windows.Forms.CheckBox checkBoxNewApiIpAddress;
        private System.Windows.Forms.CheckBox checkBoxRememberMe;
        private System.Windows.Forms.ComboBox comboBoxApiIpAddress;
    }
}

