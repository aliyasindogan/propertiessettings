﻿using System;
using System.Windows.Forms;

namespace PropertiesSettings
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            lblUserName.Text = Properties.Settings.Default["UserName"].ToString();
            lblPassword.Text = Properties.Settings.Default["Password"].ToString();
            lblApiIpAddress.Text = Properties.Settings.Default["SelectedApiIpAddress"].ToString();
        }
    }
}